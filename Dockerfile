# This image provides a base for building and running SpringBoot applications.
# It builds using maven.

FROM centos/s2i-base-centos7
MAINTAINER Stefan Stan <19stephan93@gmail.com>

EXPOSE 8080

ARG MAVEN_VERSION=3.5.3
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

LABEL io.k8s.description="Platform for building and running SpringBoot applications" \
      io.k8s.display-name="SpringBoot Platform" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder" \
      io.openshift.s2i.destination="/opt/s2i/destination" \
      maintainer="Stefan Stan <19stephan93@gmail.com>"

RUN mkdir -p /opt/s2i/destination \
  && mkdir -p /my-dir/sources \
  && mkdir -p /opt/app-root/src/.m2

# Install JAVA

RUN cd /opt \
  && wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u171-b11/512cd62ec5174c3487ac17c61aaa89e8/jdk-8u171-linux-x64.tar.gz" \
  && gunzip jdk-8u171-linux-x64.tar.gz \
  && tar xf jdk-8u171-linux-x64.tar \
  && cd jdk1.8.0_171/ \
  && alternatives --install /usr/bin/java java /opt/jdk1.8.0_171/bin/java 2 \
  && alternatives --install /usr/bin/jar jar /opt/jdk1.8.0_171/bin/jar 2 \
  && alternatives --install /usr/bin/javac javac /opt/jdk1.8.0_171/bin/javac 2 \
  && alternatives --set java /opt/jdk1.8.0_171/bin/java \
  && alternatives --set javac /opt/jdk1.8.0_171/bin/javac \
  && alternatives --set jar /opt/jdk1.8.0_171/bin/jar \
  && cd .. \
  && rm -rf jdk-8u171-linux-x64.tar.gz \
  && rm -rf jdk-8u171-linux-x64.tar

ENV JAVA_HOME /opt/jdk1.8.0_171  

# Install MAVEN 

RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn  


ENV MAVEN_HOME /usr/share/maven

RUN echo ${STI_SCRIPTS_PATH}

# Copy the S2I scripts from the specific language image to $STI_SCRIPTS_PATH
COPY ./.s2i/bin/ $STI_SCRIPTS_PATH

RUN chown -R 1001:0 /my-dir \
  && chown -R 1001:0 /opt/app-root/src/.m2 \
  && chmod -R ug+rwX /my-dir/sources \
  && chmod -R g+rw /opt/s2i/destination

USER 1001

CMD $STI_SCRIPTS_PATH/usage